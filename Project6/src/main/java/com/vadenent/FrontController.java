package com.vadenent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
@RequestMapping("/")
public class FrontController {

	
	@Autowired
	UserData newUser;
	
	@Autowired
	UserManager currentUserList;
	
	@Autowired
	UserLoginStatus userLoginStatus;
	
	@Autowired
	MovieEntry movieEntry;
	
	@Autowired
	MovieService movies;
	
	// === home page ==========================================
	@GetMapping("/home")
	public String homeView(Model model) {
		
		model.addAttribute("loginStatus", userLoginStatus);
		model.addAttribute("movies", movies);
		model.addAttribute("movieEntry", movieEntry);
		
		MovieEntry[] displayMovies = new MovieEntry[4];

		int indexMax = movies.countOfMovies();
		
		// Get 4 movies from the MovieService to make available for display
		displayMovies[0] = movies.getMovie(indexMax);
		displayMovies[1] = movies.getMovie(indexMax - 1);
		displayMovies[2] = movies.getMovie(indexMax - 2);
		displayMovies[3] = movies.getMovie(indexMax - 3);
				
		model.addAttribute("displayMovies", displayMovies);

		return "home";
	}
	
	
	
	// === login pages ==========================================
	@GetMapping("/login")
	public String loginView(Model model, LoginData loginForm) {
		
		model.addAttribute("loginStatus", userLoginStatus);

		return "login";
	}
	
	@PostMapping("/loginUpdate")
	public String loginUpdate(Model model, LoginData loginForm, RedirectAttributes redirect) {

		model.addAttribute("loginStatus", userLoginStatus);

		newUser.setUserName(loginForm.getUserName());
		newUser.setPassword(loginForm.getPassword());

		if (currentUserList.isRegistered(newUser)) {
			
			if (currentUserList.isPasswordValid(newUser)) {
	
				//set current User as logged in; place in the model (session scope)
				userLoginStatus.loginAs(newUser.getUserName());
				
				//set flash message - login successful
				redirect.addFlashAttribute("message", "You logged in successfully as '" + loginForm.getUserName() + "'");
				return "redirect:/home";

			} else {	// report invalid password
				model.addAttribute("message", "Login failed.  Try Again.");
				return "login";
			}
				
		} else {	// report the user is not registered - please register first
			model.addAttribute("message", "The 'Username' is not registered.  Please select 'New User Registration'");
			return "login";
		}
		
	}  // end method loginUpdate
	
	
	
	// === add Movie pages ==========================================
	@GetMapping("/addMovie")
	public String addMovieView(Model model, MovieEntry newMovie, RedirectAttributes redirect) {

		model.addAttribute("movieEntry", newMovie);
		model.addAttribute("loginStatus", userLoginStatus);
		
		if( ! userLoginStatus.isLoggedIn())
			model.addAttribute("message", "You must be logged in to add a new movie.");
			
		return "addMovie";
		
	}	// end method addMovieView
	

	@PostMapping("/addMovieUpdate")
	public String addMovieUpdate(Model model, MovieEntry newMovie, RedirectAttributes redirect) {

		model.addAttribute("movieEntry", newMovie);
		model.addAttribute("loginStatus", userLoginStatus);

		if (userLoginStatus.isLoggedIn()) {
			movies.addMovieEntry(newMovie);
			redirect.addFlashAttribute("message", "You just added this movie: '" + newMovie.getMovieTitle() + "'");
			return "redirect:/home";
		} else {
			model.addAttribute("message", "You must be logged in to add a new movie.");
			return "addMovie";
		}
		
	}	// end method addMovieUpdate

	
	
	// === Register a new user pages ==========================================
	@GetMapping("/register")
	public String registerView(Model model, RegisterData registerForm) {
		
		model.addAttribute("loginStatus", userLoginStatus);
		
		return "register";
	}

	@PostMapping("/registerUpdate")
	public String registerUpdate(Model model, RegisterData registerForm, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", userLoginStatus);

		newUser.setUserName(registerForm.getUserName());
		newUser.setPassword(registerForm.getPassword1());
		
		if (currentUserList.isRegistered(newUser)) { 	// User is registered already
			
			model.addAttribute("message", "This UserID " + newUser.getUserName() + " is already registered.");
			return "register";
			
		} else {
			
			if (registerForm.checkPassword()) {		// Not registered and passwords match
				currentUserList.addUser(newUser);
			//	currentUserList.listUsers();
				redirect.addFlashAttribute("message", "You registered successfully as '" + registerForm.getUserName() + "'");
				return "redirect:/home";
				
			} else {								// Not registered and passwords don't match
				
				model.addAttribute("message", "Your passwords did not match.  Try again.");
				return "register";
				
			} 

		}  
		
	}  // end of method registerUpdate


	
	
	// === logout function ==========================================
	@GetMapping("/logout")
	public String logoutView(Model model, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", userLoginStatus);
		
		userLoginStatus.logout();

		redirect.addFlashAttribute("message", "You have logged out.  Good-bye.");

		return "redirect:/home";
	}

}  // end of class FrontController

