package com.vadenent;

import java.util.TreeMap;

	
public class MovieService {

	private Integer nextMovieId;
	private TreeMap<Integer, MovieEntry> mapOfMovies;
	
	
	public MovieService() {
		
		nextMovieId = 1;
				
		this.mapOfMovies = new TreeMap<>();

		// Initialize 4 movies to start with
		MovieEntry me = new MovieEntry();
		
		// True Grit
		me.setMovieId(1);
		me.setMovieTitle("True Grit");
		me.setReleaseYear("1969");
		me.setMovieDescription("I think Mr. Rooster Cogburn is making his point.  Hmmm..."
				+ "This doesn't look like the Oklahoma Territory that I know.");
		me.setMovieImage("TrueGrit1969-01.jpg");
		me.setMovieAltText("Rooster Cogburn mounted on his horse, his pistol pulled and \r\n" + 
					"pointing at the Texas Ranger who will probably now start paying attention.");
		
		addMovieEntry(me);
		
		// Star Wars
		me.setMovieId(2);
		me.setMovieTitle("Star Wars - A New Hope");
		me.setReleaseYear("1977");
		me.setMovieDescription("Still my fovorite Star Wars movie. "
				+ "I was in high school when this one came out.");
		me.setMovieImage("StarWars-01.jpg");
		me.setMovieAltText("Luke, Leia and Hans all together escaping from the Death Star.");
		addMovieEntry(me);
		
		// Casablanca
		me.setMovieId(3);
		me.setMovieTitle("Casablanca");
		me.setReleaseYear("1942");
		me.setMovieDescription("Probably my favorite 'old' movie.  Of the 100 top movie quotes,"
				+ "4 of the top 16 listed are from Casablanca.  I bet you can name a couple.");
		me.setMovieImage("Casablanca-01.jpg");
		me.setMovieAltText("Picture of Sam playing the piano with Rick behind him - both in the cafe.");
		addMovieEntry(me);
		
		// Nobody's Fool
		me.setMovieId(4);
		me.setMovieTitle("Nobody's Fool");
		me.setReleaseYear("1994");
		me.setMovieDescription("Paul Newman, Jessica Tandy and Bruce Willis - wait, what?"
				+ "How'd Bruce Willis get in there?  I love the characters in this film.  No"
				+ " special effects, no Marvel, just some folks in Bath, New York.");
		me.setMovieImage("NobodysFool-01.jpg");
		me.setMovieAltText("Poker at the 'Horse'. Sully and Carl verbally sparring "
							+ "while the game goes on.");
		addMovieEntry(me);
						
	}
	
	
	
	public MovieEntry getMovie(Integer theKey) {
		
		MovieEntry movie = mapOfMovies.get(theKey);		// get the object reference
		
		movie = movie.getAll();							// getAll() copies the state from the movie reference
														// into a new MovieEntry object and returns that reference			
		return movie;
	}
	
	
	
	public void addMovieEntry(MovieEntry newMovie) {
		
		MovieEntry movie = newMovie.getAll();
		
		movie.setMovieId(nextMovieId);
		
		mapOfMovies.put(nextMovieId, movie);
		
		nextMovieId += 1; // increment to next movieId
	}

	
	
	public int countOfMovies() {
		return mapOfMovies.size();
	}
	
	
}
