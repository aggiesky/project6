package com.vadenent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

@SpringBootApplication
public class Project6Application {

	public static void main(String[] args) {
		SpringApplication.run(Project6Application.class, args);
	}

	
	// Beans in Request Scope
	@Bean("newUser")
	@RequestScope
	public UserData createUserData() {
		UserData newUser = new UserData();
		return newUser;
	}

	@Bean("movieEntry")
	@RequestScope
	public MovieEntry makeAnEntry() {
		MovieEntry newMovie = new MovieEntry();
		return newMovie;
	}

	
	// Beans in Application Scope
	@Bean("currentUserList")
	@ApplicationScope
	public UserManager createListOfUsers() {
		UserManager listOfUsers = new UserManager();
		return listOfUsers;
	}
	
	@Bean("movieService")
	@ApplicationScope
	public MovieService makeMovieSession() {
		MovieService ms = new MovieService();
		return ms;
	}

	
	// Beans in Session Scope
	@Bean("userLoginStatus")
	@SessionScope
	public UserLoginStatus makeUserSession() {
		UserLoginStatus userStatus = new UserLoginStatus();
		userStatus.logout();
		return userStatus;
	}
	
}
