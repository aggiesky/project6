package com.vadenent;

public class MovieEntry {

	private Integer movieId;
	private String movieTitle;
	private String releaseYear;
	private String movieDescription;
	private String movieImage;
	private String movieAltText;
	
	public MovieEntry() {
		this.reset();
	}
	
	public void reset() {
		this.movieId = 0;
		this.movieTitle = "";
		this.releaseYear = "";
		this.movieDescription = "";
		this.movieAltText = "";
		this.movieImage = "imdb.jpg";
	}

	
	// id
	public Integer getMovieId() {
		return this.movieId;
	}
	public void setMovieId(Integer id) {
		this.movieId = id;
	}
	

	// movieTitle
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	
	// releaseYear
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	// movieDescription
	public String getMovieDescription() {
		return movieDescription;
	}
	public void setMovieDescription(String movieDescription) {
		this.movieDescription = movieDescription;
	}

	// movieImage
	public String getMovieImage() {
		return this.movieImage;
	}
	public void setMovieImage(String movieImage) {
		this.movieImage = movieImage;
	}

	// movieAltText
	public String getMovieAltText() {
		return this.movieAltText;
	}
	public void setMovieAltText(String movieAltText) {
		this.movieAltText = movieAltText;
	}

	

	
	public MovieEntry getAll() {
		MovieEntry me = new MovieEntry();
		me.setMovieId(this.movieId);
		me.setMovieDescription(this.movieDescription);
		me.setMovieImage(this.movieImage);
		me.setReleaseYear(this.releaseYear);
		me.setMovieTitle(this.movieTitle);
		me.setMovieAltText(this.movieAltText);
		return me;
	}
	
	
	@Override
	public String toString() {
		return "MovieEntry [movieTitle=" + movieTitle + ", releaseYear=" + releaseYear + "]";
	}
	
	
}
